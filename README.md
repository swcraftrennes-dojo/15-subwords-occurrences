# 15 - Subwords occurrences

## Subject

Implement a function that counts the number of occurrences of a subword in a word.

A word `w` is a subword of another word `W` if all the letters of `w` can be found in the same order in `W`.

By convention, `count("", <anything>)` is 0.

Examples :

-   `count("", "anything")` -> 0
-   `count("foo", "foobar")` -> 1
-   `count("fbr", "foobar")` -> 1
-   `count("foo", "ffoo")` -> 2
-   `count("foo", "foofoo")` -> 7

Performance testing :

-   `count("aaaaaaaaaaaa", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")` -> 558383307300

## Installation

Only python3 or python2 is required

## Launching Test

```
python3 -m unittest
```

or

```
python -m unittest discover
```
